---
layout: post
title: JekyllのエンコーディングをUTF-8に設定する
category: tips
tags: [encoding, utf-8]
---
このブログは、[Jekyll](http://jekyllrb.com)を使用してます。

記事を更新する際に、下記のようなエラーが出ました。

`Liquid Exception: invalid byte sequence in UTF-8 in ...`

このエラーを解決するには、Jekyllの設定ファイル*_config.yml*に以下を追加します。

{% highlight yaml %}
encoding: utf-8
{% endhighlight %}

デフォルトでは、nilになっています。

-----

参考リンク:

1. [Configuration](http://jekyllrb.com/docs/configuration "Configuration")&nbsp;<span class="url">(jekyllrb.com)</span>