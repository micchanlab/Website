---
layout: post
title: Ctrlキーの位置をCaps Lockキーと入れ替える
category: tips
tags: [pc, emacs, keyboard]
---
Emacsを使っていると左手の小指がおかしくなりそうだったので、Ctrlキーの位置をCaps Lockキーと入れ替えました。

環境：
[Linux Mint 14](https://www.linuxmint.com/)

手順は...

まず**メニュー**から**設定**を選択し、

　↓

その中から**キーボードレイアウト**をクリックする。

　↓

4つのタブがあるので、**レイアウト**タブを開く。

　↓

右下の**オプション(O)...**をクリックし、

　↓

リストから**Ctrlキーの位置**を開く。

　↓

上から3番目の**CtrlキーとCaps Lockキーを入れ替える**にチェック。

以上で、Ctrlキーを押した時にCaps Lockがかかり、Caps Lockキーを押すとCtrlキーを押した状態になります。

少しだけ快適にEmacsを使えそうです。