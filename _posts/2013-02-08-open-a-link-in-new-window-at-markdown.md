---
layout: post
title: Markdownでリンクを新しいウィンドウに開く
category: tips
tags: [markdown]
---
リンクを作成する際は、下のように書きます。

`[リンクのテキスト](リンクのアドレス "リンクのタイトル")`

e.g. *\[Micchan Lab](http://micchan.com "Micchan Lab")*

この状態では、リンクはブラウザの同じウィンドウに開かれます。

HTMLではaタグに「target=\"_blank\"」を付けることで、新しいウィンドウにリンク先の内容が表示されます。

Markdownの記法は、このtarget属性をもたないので、直接HTMLのコードを埋め込むことで実現できます。

{% highlight html %}
<a href="http://micchan.com" target="_blank">Micchan Lab</a>
{% endhighlight %}

上記コードを埋め込んだリンク↓

<a href="http://micchan.com" target="_blank">Micchan Lab</a>&nbsp;（新しいウィンドウで開く）

Markdownに少し慣れてきました。

-----

参考リンク:

1. [markdown target=“_blank" - Stack Overflow](http://stackoverflow.com/questions/4425198/markdown-target-blank "Markdown target blank")&nbsp;<span class="url">(stackoverflow.com)</span>
