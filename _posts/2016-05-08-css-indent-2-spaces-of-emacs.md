---
layout: post
title: EmacsでCSSのインデントをスペース2つにする
category: editor
tags: [emacs, tips]
---
EmacsでCSSを書くときにインデント幅をスペース2つ分にする方法です。

環境:
[Lubuntu 15.04](https://lubuntu.net/)
と
[Gnu Emacs 24.4.1](https://www.gnu.org/software/emacs/)

まず、[MELPA](https://melpa.org/#/ "MELPA")のパッケージが入るようにします。

*~/.emacs.d/init.el*に以下を追加する。

{% highlight cl %}
;; MELPA
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)
{% endhighlight %}

書いたら、Emacsを再起動します。

次に、web-mode.elパッケージを導入します。

**M-x package-install**のあとに**RET**キーを押下。

ミニバッファが**Install package:**となるので、**web-mode**と入力し再び**RET**キーを押下します。

そして、web-mode.elパッケージの設定を*~/.emacs.d/init.el*に追加する。

{% highlight cl %}
;; web-mode.el
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js?\\'" . web-mode))

;; Indent
(setq web-mode-markup-indent-offset 2)
(setq web-mode-css-indent-offset 2)
(setq web-mode-code-indent-offset 2)
{% endhighlight %}

Emacsを再起動すると、CSSのインデントがスペース2つになります。

上記の設定はHTMLとスクリプトなどのコードのインデントも一緒に設定してます。

※ちなみに、タブは無効化しています。
{% highlight cl %}
;; タブを無効化
(setq-default indent-tabs-mode nil)
{% endhighlight %}

-----

参考リンク：

1. [web-mode.el - html template editing for emacs](http://web-mode.org/ "web-mode.el - html template editing for emacs") <span class="url">(web-mode.org)</span>
2. [Emacsのタブを無効化しスペースを挿入する - みっちゃんラボ](http://micchan.com/editor/2014/11/24/all-indentation-make-from-space-only-for-emacs/ "Emacsのタブを無効化しスペースを挿入する - みっちゃんラボ") <span class="url">(micchan.com)</span>