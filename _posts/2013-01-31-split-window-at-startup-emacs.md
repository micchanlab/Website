---
layout: post
title: Emacs起動時にウィンドウを2分割する
category: editor
tags: [emacs]
---
Emacsが起動した時に、はじめからウィンドウが2分割されていたら便利。

環境:
[Linux Mint 14](https://www.linuxmint.com/)
と
[Gnu Emacs 24](https://www.gnu.org/software/emacs/)

*~/.emacs.d/init.el*に以下を追加する。

{% highlight cl %}
;; 起動時に2分割
(setq w (selected-window))
(setq w2 (split-window w nil t))
{% endhighlight %}

1. 1行目で最初に開くウィンドウを選択
2. 2行目で分割
    * split-windowのtで左右に分割を指定