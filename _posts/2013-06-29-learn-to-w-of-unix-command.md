---
layout: post
title: ログインしているユーザ名と利用状況を表示するwコマンド
category: unix
tags: [command, w, system]
---
最近、さっぱり勉強が進んでいません。頑張らねば :(

今日は、UNIXコマンドの*w*について。

-----

## w

読み方： だぶりゅー

機能： 現在ログインしているユーザ名と、その作業内容を表示する

書式：

`w [オプション] [ユーザ名]`

オプション：

*-h* ヘッダを表示しない

*-u* 現在のプロセスとCPU時間を計算しているときに、ユーザ名の違いを無視する

*-s* ログイン時刻、JCPU、PCPUを表示しない

*-f* FROM項目を表示しない

引数：

*ユーザ名* 指定したユーザの情報のみ表示する

使用例：

（環境： [Linux Mint 14 Cinnamon](https://www.linuxmint.com/)）

Terminalを開いて、以下のコマンドを実行する。

`$ w`

すると...

{% highlight sh %}
 01:18:32 up 34 min,  2 users,  load average: 0.77, 0.35
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
micchan  tty8     :0               00:45   33:45   2:19   0.31s gnome-session --session cinnamon
micchan  pts/3    :0               01:18    0.00s  0.12s  0.00s w
{% endhighlight %}

ログイン中なのは、*micchan*で、4行目のWHAT項目に*w*とあり、いましがた端末を開いて*w*コマンドを実行している様子が分かります。

ヘッダ項目の6番目、JCPUとは、ユーザが使用しているTTYから実行されている全プロセスが使った時間。

7番目のPCPUとは、WHAT項目で記されているカレント・プロセスが使っている時間を表しています。

豆知識：

*who*、*uptime*、*ps -a*という3つのコマンドを1つに組み合わせたものです。

Larry Greenfield と Michael K. Johnsonのバージョンを基に、ほとんど全部が Charles Blake によって書き直されました。

-----

参考リンク：

1. [w (UNIX) - Wikipedia](http://ja.wikipedia.org/wiki/W_%28UNIX%29 "w (UNIX) - Wikipedia") <span class="url">(ja.wikipedia.org)</span>
2. [UNIXコマンド - w](http://www.k-tanaka.net/unix/w.php "UNIXコマンド - w") <span class="url">(www.k-tanaka.net)</span>
3. [Linuxコマンド集 【w】 ログインしているユーザー名と処理内容を表示する：ITpro](http://itpro.nikkeibp.co.jp/article/COLUMN/20060228/230990/ "Linuxコマンド集 【w】 ログインしているユーザー名と処理内容を表示する：ITpro") <span class="url">(itpro.nikkeibp.co.jp)</span>