---
layout: post
title: C言語でHello Worldプログラムを作る
category: programming
tags: [c]
---
C言語を使って、端末に「hello, world」と表示させるプログラムを作りました。

環境： [Linux Mint 14 Cinnamon](https://www.linuxmint.com/)

テキストエディタで*hello.c*というファイルを作成。

以下のコードを書く。

{% highlight c %}
#include <stdio.h>

main()
{
    printf("hello, world\n");
}
{% endhighlight %}

端末を開いて、以下のコマンドを実行しコンパイルする。

`$ gcc hello.c`

すると新たに*a.out*という実行可能なファイルができるので、

`$ ./a.out`

というコマンドを走らせる。

結果は...

`$ hello, world`

初めてのCプログラムができました。

コードの内容です。

**#include \<stdio.h\>**で標準入力ライブラリを読み込み、

**main()**という引数なしの関数を定義し、**printf**関数で文字列を印字してます。

**\n**は改行記号です。

-----

参考図書：

1. <a href="http://www.amazon.co.jp/gp/product/4320026926/ref=as_li_ss_tl?ie=UTF8&camp=247&creative=7399&creativeASIN=4320026926&linkCode=as2&tag=micchanlab-22">プログラミング言語C 第2版 ANSI規格準拠</a><img src="http://www.assoc-amazon.jp/e/ir?t=micchanlab-22&l=as2&o=9&a=4320026926" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" /> <span class="url">(www.amazon.co.jp)</span>