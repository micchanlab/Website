---
layout: post
title: AndroidでcanRead()が常にfalseを返してくる
category: android
tags: [android, file, directory]
---
Androidで保存先ディレクトリを選択できるような処理を作成中に、canRead()が常にfalseを返してくる状態になりました。

環境:
[Android Minimum SDK: API 15 | Target SDK Version: 23](https://developer.android.com/) 

端末: Nexus 5 (Android 6.0.1)

状況: AndroidManifest.xmlに「WRITE_EXTERNAL_STORAGE」のパーミッションは設定済み

原因は、実機で動作確認中に<del>アクセス権限がないディレクトリに移動したタイミング</del>(2015/12/19削除)で、アプリの権限がオフになるというものでした。

(2015/12/19追記)上記を訂正します。アクセス権限がないディレクトリに移動したタイミングではなく、「設定」の「ストレージ」から「データを消去」したタイミングでした。

端末にある「設定」から、「アプリ」をタップし、該当のアプリを再度タップすると「許可」という項目があるので、その中の「ストレージ」をオンに戻すと解決します。