---
layout: post
title: Emacsを起動時に最大化する
category: editor
tags: [emacs]
---
*~/.emacs.d/init.el*に以下を追加
{% highlight cl %}
;; スクリーンの最大化
(set-frame-parameter nil 'fullscreen 'maximized)
{% endhighlight %}
ちなみに、フルスクリーンにするには...
{% highlight cl %}
;; フルスクリーン
(set-frame-parameter nil 'fullscreen 'fullboth)
{% endhighlight %}