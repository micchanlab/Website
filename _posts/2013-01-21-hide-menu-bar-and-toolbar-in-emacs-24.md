---
layout: post
title: Emacsのメニューバーとツールバーを非表示にする
category: editor
tags: [emacs]
---
環境: 
[Linux Mint 14](https://www.linuxmint.com/)
と
[GNU Emacs 24](https://www.gnu.org/software/emacs/)

*~/.emacs.d/init.el*に以下を追加
{% highlight cl %}
;; メニューバーを非表示
(menu-bar-mode 0)
;; ツールバーを非表示
(tool-bar-mode 0)
{% endhighlight %}

[A Guided Tour of Emacs](http://www.gnu.org/software/emacs/tour/)(公式サイトのガイドツアー)
