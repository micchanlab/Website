---
layout: post
title: Emacsで現在時刻を入力する
category: editor
tags: [emacs, tips]
---
アプリを作っているときにGNU Emacsの[Org mode](https://orgmode.org/ja/index.html "Org mode")でメモを残しています。

ログのような作りにしていて、何時何分にこれをしていたということが後から見返してもわかるように記録しています。

そのため頻繁に現在時刻を入力するのですが、時計を確認して「12:34」のようにその都度入力するよりも、下記のように設定しておくと素早く入力することができます。

環境: [Debian GNU/Linux 11 (bullseye)](https://www.debian.org/ "Debian GNU/Linux 11 (bullseye)") + [GNU Emacs 27.1](https://www.gnu.org/software/emacs/ "GNU Emacs")

*~/.emacs.d/init.el*に以下を追加する。

{% highlight elisp %}
;; 現在時刻の挿入
(defun now ()
  "Insert current time."
  (interactive)
  (insert (format-time-string "%-H:%M")))
(global-set-key (kbd "C-c C-:") 'now)
{% endhighlight %}

これで*C-c C-:*を押下したら「2:34」のような時刻がカーソル下に挿入されます。

時刻を太字にしたい場合はフォーマット部分に*\*%-H:%M\**とアスタリスクを前後に追加します。

以上です。