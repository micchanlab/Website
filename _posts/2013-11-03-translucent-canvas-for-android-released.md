---
layout: post
title: 写真をなぞってお絵描きできる半透明キャンバス
category: android
tags: [drawing, canvas]
---
というandroid向けアプリを作りました。

写真の上に半透明のキャンバスが表示されるので、タッチペンなどを使って、なぞるようにお絵かきができます。

絵が苦手な人でも、ある程度上手に描けたりします。

-----

[![半透明キャンバス](/assets/images/photos/2013-11-03/ic_launcher.png)](https://play.google.com/store/apps/details?id=com.micchan.translucentcanvas)

アプリ名： 半透明キャンバス

価格： 無料

-----

是非一度使ってみて下さい！！

ダウンロードは[Google Play](https://play.google.com/store/apps/details?id=com.micchan.translucentcanvas)で！